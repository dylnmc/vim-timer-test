
set shellslash " needed for windows
let $VIMDIR = $HOME.
\ (has('win32') ? '/vimfiles' : has('nvim') ? '/.config/nvim' : '/.vim')

filetype plugin indent on

" options
set hidden
set ignorecase
set laststatus=2
set mouse=
set noshowmode
set nostartofline
set number relativenumber
set splitbelow splitright
set tabstop=4 softtabstop=4 shiftwidth=0
set textwidth=80
set tildeop
set ttimeoutlen=20
set wildcharm=<c-z>
if has('cmdline_hist')
	set history=10000 " max
endif
if has('cmdline_info')
	set showcmd
endif
if has('extra_search')
	set hlsearch incsearch
endif
if has('folding')
	set foldlevel=1
	set foldmethod=syntax
endif
if has('syntax')
	set synmaxcol=512
	set nocursorline
	syntax enable
endif
if has('wildmenu')
	set wildmenu
endif

" create undo, backup, swap
if !isdirectory($VIMDIR.'/undo')   | call mkdir($VIMDIR.'/undo', 'p')   | endif
if !isdirectory($VIMDIR.'/backup') | call mkdir($VIMDIR.'/backup', 'p') | endif
if !isdirectory($VIMDIR.'/swap')   | call mkdir($VIMDIR.'/swap', 'p')   | endif
" undo
if has('persistent_undo')
	set undodir=$VIMDIR/undo//
	set undofile
endif
" backup
set backupdir=$VIMDIR/backup//
set backup
" swap
set directory=$VIMDIR/swap//
set swapfile

" set viminfo file to be inside $VIMDIR
execute 'set viminfo+=n'.$VIMDIR.'/viminfo'
exec 'set viminfofile='.$VIMDIR.'/viminfo'

" syntax-based folding (need :set fdm=syntax)
let g:ada_folding               = 'ig'
let g:baan_fold                 = 1
let g:clojure_fold              = 1
let g:fortran_fold              = 1
let g:fortran_fold_conditionals = 1
let g:ft_man_folding_enable     = 1
let g:javaScript_fold           = 1
let g:perl_fold                 = 1
let g:php_folding               = 1
let g:r_syntax_folding          = 1
let g:ruby_fold                 = 1
let g:rust_fold                 = 1
let g:sh_fold_enabled           = 3
let g:tex_fold_enabled          = 1
let g:tex_fold_enabled          = 1
let g:vimsyn_folding            = 'afP'
let g:vimtex_fold_manual        = 0
let g:vimtex_folding            = 1
let g:vimtex_toc_fold           = 1
let g:xml_syntax_folding        = 1
let g:zsh_fold_enable           = 1


" if --noplugin flag used, finish
if (!&loadplugins)
	finish
endif

" dylnmc/lyne
let g:lyne_active = {
\	'left': [
\		'h:LyneMode', 'r:lyne#get_mode',
\		'h:LyneFname', 'e:%{expand(''%:p:~:.'')}%<%h%w%m%r',
\		'h:StatusLine', 'e:%{&filetype}'
\	],
\	'right': ['h:LyneMode', 'e:%p%%  %2v']
\}
let g:lyne_inactive = {
\	'right': ['e:%p%%']
\}
let g:lyne_separators = {'left': '', 'right': ''}

" plug setup
" auto-create autoload directory
if !isdirectory($VIMDIR.'/autoload')
	call mkdir($VIMDIR.'/autoload', 'p')
endif

" if plug.vim not in autoload directory...
if empty(glob($VIMDIR.'/autoload/plug.vim'))

	function! s:vimReload()
		source $MYVIMRC
		redraw!
	endfunction

	function! s:plugWarnSetup()
		echohl WarningMsg
		if (! executable('curl'))
			" warn that curl isn't installed and notify about :PlugDownload
			echon 'curl is not installed; install it, then run :PlugDownload'
			echohl None
		else
			" warn that plug not installed and notify about :PlugDownload
			echon 'vim-plug is not installed; run :PlugDownload to install it'
		endif
		echohl None

		" create :PlugDownload
		command! -nargs=0 -bar PlugDownload
		\ call s:plugCurl() <bar> redraw! <bar> call s:vimReload() <bar>
		\ PlugInstall --sync <bar> silent! call lyne#update() <bar>
		\ call s:vimReload()
	endfunction

	function! s:plugCurl()
		" curl plug into $VIMDIR/autoload/plug.vim
		execute 'silent !curl -fLo '.$VIMDIR.'/autoload/plug.vim --create-dirs'
		\ 'https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
	endfunction

	augroup PlugDownload
		autocmd!

		" warn about plug on VimEnter since it's not isntalled
		autocmd VimEnter * call s:plugWarnSetup()

	augroup end

	" since vim-plug is not installed, exit
	finish

endif

" uncomment next line if the there are a lot of erors with :PlugInstall
" let g:plug_threads = 5

" plug
call plug#begin($VIMDIR.'/plugged')

" gitlab
let g:plug_url_format = 'https://gitlab.com/%s.git'

Plug 'dylnmc/lyne-vim'

unlet g:plug_url_format

call plug#end()

