
if ! exists('s:xmasNotInit')
	let s:xmasNotInit = 1
	let s:term = 1
	let s:state = 1
	let s:guiNum = 0
endif

function! s:nrs2hex(r, g, b)
	return '#'.printf('%02x', a:r) . printf('%02x', a:g) . printf('%02x', a:b)
endfunction

function! xmas#XMas(timer)
	if s:term
		if s:state > 0
			hi XMas guibg=#BF616A guifg=#A3BE8C ctermbg=1 ctermfg=2
		else
			hi XMas guibg=#A3BE8C guifg=#BF616A ctermbg=2 ctermfg=1
		endif
	else
		exec 'hi XMas guibg='.s:nrs2hex(s:guiNum, 255-s:guiNum, 0).
		\ ' guifg='.s:nrs2hex(255-s:guiNum, s:guiNum, 0)
		let s:guiNum += s:state
	endif
	if s:xmasNotInit
		let s:xmasNotInit = 0
		hi XMas gui=bold cterm=bold
		echohl XMas
		echon 'MERRY X-MAS!'
		echohl NONE
		call extend(g:lyne_active.right, ['h:XMas', 'e:Merry X-Mas WOOT!'], 0)
		call lyne#setup()
		call lyne#update()
		call lyne#post()
	endif
	if s:term || (s:guiNum + s:state > 255 && s:state > 0) || (s:guiNum + s:state < 0 && s:state < 0)
		let s:state = s:state * -1
	endif
endfunction

function xmas#XMasStart()
	if exists('s:XMasTimer')
		return
	endif
	if exists('g:xmas_term')
		let s:term = g:xmas_term
	else
		let s:term = ! (has('gui_running') || &termguicolors)
	endif
	let s:state = s:term ? 1 : 10
	call xmas#XMas(0)
	let s:XMasTimer = timer_start(s:term ? 1000 : 100, function('xmas#XMas'), {'repeat':-1})
endfunction

function! xmas#BaHumbug()
	if ! exists('s:XMasTimer')
		echohl ErrorMsg
		unsilent echon 'Yeah! Ba HUMBUG!'
		echohl NONE
		return
	endif
	call timer_stop(s:XMasTimer)
	unlet s:XMasTimer
	let s:xmasNotInit = 1
	if (g:lyne_active.right[0] ==# 'h:XMas')
		call remove(g:lyne_active.right, 0, 1)
	endif
	call lyne#setup()
	call lyne#update()
	call lyne#post()
endfunction

